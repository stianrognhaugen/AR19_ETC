﻿Schematics
    v1
      Opprettet første design.
    v2
      Endret test signal direkte 
    v3
      ...
    v4
      Satt 1k pull-ups på LM239 utganger.
      10 Pull-down på MOSFET Gate test LED
      10k pulldown på MOSFET Gate relé
      10k pull-ups på sensor in signal. Porten blir høy hvis signalet faller ut.
      P-Mosfet med kondensator for å resette ved cycling av strøm.
      Diode mellom TEST-linjen og TPS slik at test-LED ikke lyser når TPS er aktivert.
      Reset funksjon med P-MOSFET, 0.1uF kondensator og 10k motstand. Koblet i serie med tre reset pins på latchen.
      LM239 er nå koblet til supply på 12V for å kunne sammenligne over 3.5 V.
      LM239-3 og -4 sammenligner signalspenning med ref på 4.85V (R10k-50k-GND)
      To nye N-Mosfets i parallell for å åpne releet hvis signalet dropper. Koblet med diode til gul LED slik at denne lyser hvis signalet dropper ut. Gate til latch. 
      Satt på 10uF kondensatore på spenningsregulatoren. 
    v5
      Endret input IC1C til TPS_IN med 10k pullup.
      Endret input IC1D til BPS_IN med 10k pullup.
      Fjernet P-MOSFET til reset funksjon.
      Fjernet D2 og D3. YLW LED lyser ikke lenger ved signal drop.
      Lagt til 555-timer som monostable multivibrator (R100, R1k, C10u og C100n) for PoR
    v6
      Erstattet alle MOSFETs med BJT transistorer. 
      Lagt til blå LED for indikator av power on
      Endret R2 fra 50k til 11k for å gi en referansespenning på 4.58V
    v7
      Endret BJT tilbake til MOSFETs i SOT-23 (BSS138).
      Fjernet AND-gate
      Brukt Wired-AND og OR på utgangene til LM239. 

PCB
    RevA.
      Opprettet første design. 
      Lagt til 2pin header for av- og påkobling av
      Økt størrelse på hull til Ø3.2mm for montering med M3.
    RevB.
      Designet PCB iht. oppdateringene fra Schematics v4. 
    RevC.
      Designet PCB iht. oppdateringene fra Schematics v5. 
