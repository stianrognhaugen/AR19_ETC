#include <Arduino.h>
#include <stdint.h>
#include "tps.h"

int16_t TPS::tpsRead(uint8_t tps_pin) {

  //  Reading and re-mapping TPS values
  int16_t tps_value = analogRead(tps_pin);
  tps_value = map(tps_value, 0, 1023, 0, 100);

  return tps_value;

}
