//  Include libraries
#include <Arduino.h>
#include <stdint.h>
#include "apps.h"
//#include "tps.h"
//#include "pid.h"
//#include "pwm.h"

//  For Arduino Uno

//  PINS
const uint8_t enable_pin   = 4;     //  Motor's enable pin
const uint8_t DCdir1_pin   = 10;    //  Direction 1
const uint8_t DCdir2_pin   = 11;    //  Direction 2
const uint8_t apps1_pin    = A0;    //  APPS #1
const uint8_t apps2_pin    = A1;    //  APPS #2
const uint8_t tps1_pin     = A2;    //  TPS #1, 0-5V
const uint8_t tps2_pin     = A3;    //  TPS #2, 5-0V
//  DC ETB
int16_t desiredETBvoltage = 0;      //  Desired ETB voltage
//  TPS
int16_t tps1_value        = 0;     //  TPS #1 current value
int16_t tps1_value_old    = 0;     //  TPS #1 old value
int16_t tps2_value        = 0;     //  TPS #2 current value
int16_t tps2_value_old    = 0;     //  TPS #2 old value
//  APPS
int16_t apps1_value       = 0;     //  APPS #1 current value
int16_t apps1_value_old   = 0;     //  APPS #1 memory value
int16_t apps2_value       = 0;     //  APPS #2 current value
int16_t apps2_value_old   = 0;     //  APPS #2 memory value
//  PID
double sp                 = 0;     //  PID setpoint
double dt                 = 0;     //  Loop interval time
double Kp                 = 0;     //  Proportional term
double Ki                 = 0;     //  Integral term
double Kd                 = 0;     //  Derivative term

uint8_t DC_pin            = 0;

void setup() {

  //  Inputs
  pinMode(tps1_pin, INPUT);
  pinMode(tps2_pin, INPUT);
  pinMode(apps1_pin, INPUT);
  pinMode(apps2_pin, INPUT);

  //  Outputs
  pinMode(DCdir1_pin, OUTPUT);
  pinMode(DCdir2_pin, OUTPUT);
  pinMode(enable_pin, OUTPUT);

  Serial.begin(9600);             //  For printing to serial monitor

}

void loop() {

  //  Reading and re-mapping APPS values
  APPS APPSRead;
  apps1_value = APPSRead.appsRead(apps1_pin);
  //apps2_value = APPSRead.appsRead(apps2_pin);

  //  Prints the APPS1 value
  //  Serial.println(apps1_value);
  //  Serial.println(apps2_value);

  // Evaluating the direction of the first APPS
  APPS APPSDirecion;
  bool dir_apps1 = APPSDirecion.appsDirection(apps1_value, apps1_value_old);
  // bool dir_apps2 = APPSDirecion.appsDirection(apps2_value, apps2_value_old);

  // //  Prints the direction of the APPS to the serial monitor
  // Serial.println(dir_apps1);
  // Serial.println(dir_apps2);

  //  Reading and re-mapping TPS values
  // TPS TPSRead;
  // tps1_value = TPSRead.tpsRead(tps1_pin);
  // tps2_value = TPSRead.tpsRead(tps2_pin);

  // //  PID
  // PID etbPID;
  // etbPID.PIDsignal(sp, dt, Kp, Ki, Kd);
  //
  // //  PWM
  // PWM etbPWM;
  // etbPWM.etbPWM(ETBvoltage);

  //  ETB direction
  digitalWrite(enable_pin, HIGH);

  //  PWM test
  desiredETBvoltage = apps1_value;

  // PWM PWMtest;
  // PWMtest.etbPWM(desiredETBvoltage, DCdir1_pin);

  //  if(dir_apps1 = true){
  //    DC_pin = DCdir1_pin;
  //  }
  //  else if(dir_apps1 = false) {
  //    DC_pin = DCdir2_pin;
  //  }

  switch(dir_apps1){
    case 0: 
      DC_pin = DCdir2_pin;
      break;
    case 1:
      DC_pin = DCdir1_pin;
      break;
  }

  analogWrite(DC_pin, desiredETBvoltage);

  delay(10);

  // Storing current APPS value for comparison with the next APPS value
  apps1_value_old = apps1_value;
  // apps2_value_old = apps2_value;

}
