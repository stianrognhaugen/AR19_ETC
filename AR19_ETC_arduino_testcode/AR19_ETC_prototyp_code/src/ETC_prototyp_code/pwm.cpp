#include <Arduino.h>
#include <stdint.h>
#include "pwm.h"

void PWM::etbPWM(int16_t desiredETBvoltage, uint8_t DCdir_pin) {

  //  PWM OUTPUT
  analogWrite(DCdir_pin, desiredETBvoltage);
  Serial.println(desiredETBvoltage);

}
