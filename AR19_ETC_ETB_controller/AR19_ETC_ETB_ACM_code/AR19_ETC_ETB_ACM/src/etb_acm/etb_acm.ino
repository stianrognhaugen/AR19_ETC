//  Include libraries
#include <Arduino.h>
#include <stdint.h>
#include "tps.h"
#include "pid.h"
#include "pwm.h"

//  PINS
const uint8_t enable_pin = AD2_SDA;   //  Motor's enable pin
const uint8_t DCdir1_pin = PWM0;      //  Direction 1
const uint8_t DCdir2_pin = PWM1_INT1; //  Direction 2
const uint8_t tps1_pin   = AD0;       //  TPS #1, 0-5V
const uint8_t tps2_pin   = AD1;       //  TPS #2, 5-0V
//  DC ETB
double ETBsignal        = 0;          //  Desired ETB voltage
//  TPS
int16_t tps1_value       = 0;         //  TPS #1 current value
int16_t tps1_value_old   = 0;         //  TPS #1 old value
int16_t tps2_value       = 0;         //  TPS #2 current value
int16_t tps2_value_old   = 0;         //  TPS #2 old value
//  PID
const double sp          = 0;         //  PID setpoint
const double dt          = 0;         //  Loop interval time
const double Kp          = 0;         //  Proportional term
const double Ki          = 0;         //  Integral term
const double Kd          = 0;         //  Derivative term

void setup() {

  //  Inputs
  pinMode(tps1_pin, INPUT);
  pinMode(tps2_pin, INPUT);

  //  Outputs
  pinMode(DCdir1_pin, OUTPUT);
  pinMode(DCdir2_pin, OUTPUT);
  pinMode(enable_pin, OUTPUT);

}

void loop() {

  //  Reading and re-mapping TPS values
  TPS TPSRead;
  tps1_value = TPSRead.tpsRead(tps1_pin);
  tps2_value = TPSRead.tpsRead(tps2_pin);

  //  PID
  PID etbPID;
  etbPID.PIDsignal(sp, dt, Kp, Ki, Kd);

  //  PWM
  PWM etbPWM;
  etbPWM.etbPWM(ETBsignal);

  delay(10);

}
