
TPS SENSORS

	Pot1 leser fra 0 til 5 V for 0 til 90 grader
	Pot2 leser fra 5 til 0 V for 0 til 90 grader

	Ved idle er analogRead for Pot1 191
	Ved idle er analogRead for Pot2 829

	Virker som om den i praksis leser fra 0,5 til 4,5 V, som tilsvarer 101,4 til 920,6 ved analogRead i helt lukket posisjon