#ifndef APPS_H
#define APPS_H

class APPS {
public:
  int16_t appsRead(uint8_t apps_pin);
  bool appsDirection(int16_t apps_value, int16_t apps_value_old);

private:
  int16_t diff;

};

#endif
