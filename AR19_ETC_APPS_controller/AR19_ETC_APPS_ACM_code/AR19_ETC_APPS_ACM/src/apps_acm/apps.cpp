#include <Arduino.h>
#include <stdint.h>
#include "apps.h"

int16_t APPS::appsRead(uint8_t apps_pin) {

  //  Reading and re-mapping APPS values
  int16_t apps_value;
  apps_value = analogRead(apps_pin);
  apps_value = map(apps_value, 0, 1023,0,100);

  //  Returning the APPS value
  return apps_value;

}

bool APPS::appsDirection(int16_t apps_value, int16_t apps_value_old) {

  //  APPS Direction Variables
  bool direction;
  diff = apps_value - apps_value_old;

  //  Evaluating which direction the APPS is moving
  if (diff > 0) {
  direction = 1;          //  Acceleration
  }
  else if (diff <= 0) {
  direction = 0;          //  Deceleration
  }

  //  Returning the direction of the APPS
  return direction;

}
