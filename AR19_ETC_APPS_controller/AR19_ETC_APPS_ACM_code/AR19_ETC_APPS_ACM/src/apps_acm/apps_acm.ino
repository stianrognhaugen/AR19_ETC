//  Include libraries
#include <Arduino.h>
#include <stdint.h>
#include "apps.h"

//  PINS
const uint8_t apps1_pin  = A0;    //  APPS #1
const uint8_t apps2_pin  = A1;    //  APPS #2
//  APPS
int16_t apps1_value      = 0;     //  APPS #1 current value
int16_t apps1_value_old  = 0;     //  APPS #1 memory value
int16_t apps2_value      = 0;     //  APPS #2 current value
int16_t apps2_value_old  = 0;     //  APPS #2 memory value

void setup() {

  //  Inputs
  pinMode(apps1_pin, INPUT);
  pinMode(apps2_pin, INPUT);

}

void loop() {

  //  Reading and re-mapping APPS values
  APPS APPSRead;
  apps1_value = APPSRead.appsRead(apps1_pin);
  apps2_value = APPSRead.appsRead(apps2_pin);

  // Evaluating the direction of the first APPS
  APPS APPSDirecion;
  bool dir_apps1 = APPSDirecion.appsDirection(apps1_value, apps1_value_old);
  bool dir_apps2 = APPSDirecion.appsDirection(apps2_value, apps2_value_old);

  //  Storing current APPS value for comparison with the next APPS value
  apps1_value_old = apps1_value;
  apps2_value_old = apps2_value;

  //  Send APPS values over CAN


}
