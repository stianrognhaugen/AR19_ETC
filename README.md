<h1 align="center"> Align Racing 2019 Electronic Throttle Control </h1> <br>
<p align="center">
    <a href="http://www.alignracing.no/">
      <img alt="Align Racing UiA" title="Align Racing UiA" src="https://i.imgur.com/idweYeR.png" width="400">
    </a>
</p>

<!-- START doctoc generate TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of contents

- [Introduction](#introduction)
- [Align Racing UiA](#alignracinguia)
- [Contributors](#contributors)
- [Acknowledgments](#acknowledgments)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

This is the repository for Align Racing 2019 Electronic Throttle Control bachelor thesis. The goal of the project is to develop and implement an electronic throttle control system for a Formula Student race car.


## Align Racing UiA

<a href="http://www.alignracing.no/">Align Racing UiA (AR)</a> is a multidisciplinary student organisation at <a href="https://www.uia.no/">The University of Agder (UiA)</a>, that take part in the world’s largest yearly engineering competition; <a href="https://www.imeche.org/events/formula-student">Formula Student (FS)</a>. The organisation continuously consists of around 60 students, from a wide variety of disciplines. The competition includes developing, building, budgeting and marketing a small scale formula type racing car. AR takes this a bit further by having more focus on developing a strong organisation, open for all disciplines, that gives the members a taste of future work-life.


## Contributors

<div align="center">
<a href="https://github.com/stianrognhaugen" target="_blank">
    <img src="https://i.imgur.com/BnVEktU.png" alt="Stian Rognhaugen">
</a>

<a href="https://github.com/sandmag" target="_blank">
    <img src="https://i.imgur.com/azzeeiS.png" alt="Sander Bråten Johannessen">
</a>

<a href="https://github.com/gurgle96" target="_blank">
    <img src="https://i.imgur.com/SNUffRw.png" alt="Jørgen Nilsen">
</a>
</div>

## Acknowledgments

Special thanks to our supervisor <a href="https://www.uia.no/kk/profil/kristimk">Kristian Muri Knausgård</a> for helping and supporting our project.
